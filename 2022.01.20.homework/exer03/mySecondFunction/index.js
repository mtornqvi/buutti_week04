module.exports = async function (context, req) {
    context.log("Second function triggered");

    try {
        const animal = context.req.body;
        const yearOfBirth = (new Date().getFullYear() - Number(animal.age));
        animal.yearOfBirth = yearOfBirth;
        delete animal.age;
        context.res = {
            status: 200,
            body: animal
        };
    } catch (error) {
        context.res = {
            status: 400,
            body: error.message
        };
    }
};
