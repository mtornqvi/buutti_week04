## Implements two Azure functions which talk to each other

## Initialize local
npm init -y

## In Visual studio code => Azure extension
Create local project
Create two functions (http response)

## Start functions locally
npm install
npm start

## Install axios (at project root)
npm install axios --save


## Javascript points
### try/catch -structure
### Checking if an array has a property
const keys = ["animal", "color", "age"];

if (!keys.every(key => animal.hasOwnProperty(key))) {

### awaiting update from secondfunction
const updatedAnimal = await axios.post(url, animal);

