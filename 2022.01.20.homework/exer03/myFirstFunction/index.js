const axios = require("axios");
// const url = "http://localhost:7071/api/mySecondFunction";
const url = "https://mikko-exer03-app.azurewebsites.net/api/mysecondfunction";

module.exports = async function (context, req) {
    context.log("First function triggered.");

    const keys = ["animal", "color", "age"];
    try {
        const animal = req.body;
        if (typeof animal === "undefined") {
            throw { message: "Empty object. Required keys are animal, color and age" };
        }
        // I'm fine with there being unwanted keys, as long as the ones I want are there
        if (!keys.every(key => animal.hasOwnProperty(key))) {
            throw { message: "invalid keys. Required keys are animal, color and age" };
        }
        if (!Number(animal.age) || (typeof animal.age === "boolean")) {
            throw { message: "age has to be a number" };
        }
        const updatedAnimal = await axios.post(url, animal);

        context.res = {
            status: 200,            
            body: updatedAnimal.data
        };
    } catch (error) {
        context.res = {
            status: 400,
            body: error.message
        };
    }



    
};